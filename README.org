* Data Science with R - Apuntes del curso
Estos son mis apuntes del curso de ciencia de datos con R, de HarvardX, en la plataforma edX.

Profesor: Miguel Irizarri.

** Temario
El curso consta de 9 módulos, a saber:

1. R Basics
2. Visualization
3. Probability
4. Inference and Modelling
5. Productivity Tools
6. Wrangling
7. Linear Regression
8. Machine Learning
9. Capstone

* Changelog

** DONE [4/4] Migrar módulo 1 a RMarkdown
CLOSED: [2024-04-14 dom 21:09]
- [X] Hay una lista enorme de números en las páginas 3, 4 y 5. ¿Está =x= mal definida? No, hereda el valor de códigos posteriores en el documento cuando se ejecuta por segunda vez.
- [X] Eliminar todas las variables del espacio de trabajo al principio.
- [X] Eliminar encabezado para /Footnotes/.
- [X] Concluir el apartado de /Programación básica/
  - [X] Introducción
  - [X] Estructuras condicionales
  - [X] Bucles
  - [X] Funciones

** IN_PROGRESS Terminar módulo 2

** TODO Migrar módulo 3 a R-Markdown
** TODO Migrar módulo 4 a R-Markdown
** TODO Migrar módulo 5 a R-Markdown
** TODO Migrar módulo 6 a R-Markdown
** TODO Migrar módulo 7 a R-Markdown
** TODO Terminar módulo 8
